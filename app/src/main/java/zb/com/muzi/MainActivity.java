package zb.com.muzi;

import android.support.v4.app.Fragment;


public class MainActivity extends BaseSingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return MainFragment.newInstance();
    }
}

