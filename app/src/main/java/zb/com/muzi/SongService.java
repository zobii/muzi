package zb.com.muzi;

import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.List;

/**
 * Created by zohaibhussain on 2016-09-09.
 */

public class SongService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener{

    private final IBinder songBinder = new SongServiceBinder();
    MediaPlayer mPlayer;
    List<Song> mSongList;
    int mSongIndex;

    @Override
    public void onCreate() {
        super.onCreate();
        mPlayer = new MediaPlayer();
        mSongIndex = 0;
        initMediaPlayer();
    }

    private void initMediaPlayer() {
        mPlayer.setWakeMode(this, PowerManager.PARTIAL_WAKE_LOCK);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnErrorListener(this);
        mPlayer.setOnCompletionListener(this);
    }

    public void setSongList(List<Song> songList) {
        mSongList = songList;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return songBinder;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

    }

    @Override
    public boolean onUnbind(Intent intent) {
        mPlayer.stop();
        mPlayer.release();
        return false;
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }

    public void playSong() {
        mPlayer.reset();
        Song playSong = mSongList.get(mSongIndex);
        long currSongID = playSong.getId();
        Uri songURI = ContentUris.withAppendedId(
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                currSongID);
        try {
            mPlayer.setDataSource(getApplicationContext(), songURI);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlayer.prepareAsync();
    }

    public void setSong(int songIndex) {
        mSongIndex = songIndex;
    }

    public class SongServiceBinder extends Binder {
        SongService getService() {
            return SongService.this;
        }
    }
}
