package zb.com.muzi;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by zohaibhussain on 2016-09-08.
 */

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongViewHolder>{

    Context mContext;
    List<Song> mSongList;

    SongListClickListener mSongClickListener;


    public SongAdapter(Context mContext, List<Song> mSongList) {
        this.mContext = mContext;
        this.mSongList = mSongList;
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new SongViewHolder(inflater.inflate(R.layout.song_entry, parent, false));
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        holder.bind(mSongList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mSongList.size();
    }

    public SongListClickListener getSongClickListener() {
        return mSongClickListener;
    }

    public void setSongClickListener(SongListClickListener songClickListener) {
        this.mSongClickListener = songClickListener;
    }

    public interface SongListClickListener {
        void onSongClicked(int position);
    }

    protected class SongViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView songTitle;
        TextView songArtist;
        int index;

        public SongViewHolder(View itemView) {
            super(itemView);
            songTitle = (TextView) itemView.findViewById(R.id.song_title_textview);
            songArtist = (TextView) itemView.findViewById(R.id.song_artist_textview);
            itemView.setOnClickListener(this);
        }

        public void bind(Song song, int index) {
            songTitle.setText(song.getTitle());
            songArtist.setText(song.getArtist());
            this.index = index;
        }

        @Override
        public void onClick(View view) {
            if (mSongClickListener != null)
                mSongClickListener.onSongClicked(index);
        }
    }
}
