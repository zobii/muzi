package zb.com.muzi;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zohaibhussain on 2016-09-08.
 */

public class MainFragment extends Fragment {

    private static final int REQUEST_EXTERNAL_STORAGE_PERMISSION = 1;

    @BindView(R.id.song_recycler_view)
    RecyclerView mSongRecyclerView;

    List<Song> mSongList;
    SongService mSongService;
    Intent mPlaySongIntent;
    boolean mSongServiceBound;
    ServiceConnection mSongServiceConnection;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSongList = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_PERMISSION);
        }else
            initializeMuzi();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_EXTERNAL_STORAGE_PERMISSION)
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initializeMuzi();
                initializeRecycler();
            }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        initializeRecycler();
        return view;
    }

    private void initializeMuzi(){
        fetchSongsFromDevice();
        connectSongService();
        initializeSongService();
    }

    private void fetchSongsFromDevice() {
        ContentResolver musicResolver = getActivity().getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
        if(musicCursor!=null && musicCursor.moveToFirst()){
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            do {
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                mSongList.add(new Song(thisId, thisTitle, thisArtist));
            }
            while (musicCursor.moveToNext());
            musicCursor.close();
        }
    }

    private void connectSongService() {
        mSongServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder serviceBinder) {
                SongService.SongServiceBinder binder = (SongService.SongServiceBinder) serviceBinder;
                mSongService = binder.getService();
                mSongService.setSongList(mSongList);
                mSongServiceBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                mSongServiceBound = false;
            }
        };
    }

    private void initializeRecycler() {
        mSongRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mSongRecyclerView.setAdapter(new SongAdapter(getActivity(), mSongList));
        ((SongAdapter) mSongRecyclerView.getAdapter()).setSongClickListener(new SongAdapter.SongListClickListener() {
            @Override
            public void onSongClicked(int position) {
                mSongService.setSong(position);
                mSongService.playSong();
            }
        });
    }

    private void initializeSongService() {
        if (mPlaySongIntent == null) {
            mPlaySongIntent = new Intent(getActivity(), SongService.class);
            getActivity().bindService(mPlaySongIntent, mSongServiceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onDestroy() {
        getActivity().unbindService(mSongServiceConnection);
        mSongService = null;
        super.onDestroy();
    }
}
